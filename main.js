var mqtt = require('mqtt');
const fs = require('fs');
const express = require('express');
const app = express();
const port = 3000;

var mqtt_broker = 'mqtt://172.16.153.2';
if (process.env['MQTT_BROKER']) {
  mqtt_broker = process.env['MQTT_BROKER'];
}

const client = mqtt.connect(mqtt_broker, {
  clientId: 'mqtt-state-store'
});

var state_file = "state.json";
if (process.env['STATE_FILE']) {
  state_file = process.env['STATE_FILE']
};

var topic = "state/#";
if (process.env['TOPIC']) {
  topic = process.env['TOPIC']
};

var state = {};

try {
  let str = fs.readFileSync(state_file);
  if (str && str != "") {
    state = JSON.parse(str);
    console.log("Read persisted state from file", state_file);
    console.log("Current state: ", state);
   }
} catch (e) {
  console.log("State was not preloaded from disk");
}

function saveState() {    
  fs.writeFile(state_file, JSON.stringify(state, null, 4) + "\n", function(err) {
    if(err) {
      return console.log(err);
    }
  });     
}

client.on('connect', function () {
  client.subscribe(topic, { nl: true }, function (err) {
    if (!err) {
//    client.publish('presence', 'Hello mqtt')
      console.log("Subscribed to", topic);
    }
  })
})
 
client.on('message', function (topic, message) {
  // message is Buffer

  console.log("topic:", topic, "message:", message.toString());
  
  if (topic.endsWith("/value")) {
    return;
  }
  if (topic.endsWith("/set")) {
    // remove the /set from the topic
    const key = topic.substring(0, topic.length - "/set".length);
    let str = message.toString();
    if (!isNaN(str)) {
      state[key] = parseFloat(str);
    } else {
      state[key] = str;
    }
    client.publish(key + "/value", message.toString(), {retain: true});
    saveState();  
  }
});

app.get('/', function(req, res) {
  res.type('application/json');
  res.send(JSON.stringify(state, null, 4));
});

app.get('*', function (req, res) {
  let url = req.url.substr(1);
  if (state[url]) {
    res.send(state[url] + "\n");
  } else {
    res.status(404);
    res.send("Not found\n");
  }
});

// Push every current value back to the same topic
setInterval(() => {
  for (let [key, value] of Object.entries(state)) {
    let topic = key + '/value';
    client.publish(topic, value.toString(), {retain: true});
    console.log('refreshing:', topic, value.toString());
  }
}, 30000);

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

